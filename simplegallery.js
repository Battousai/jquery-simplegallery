!function ($) {

	var SimpleGallery = function (element, options) {
		this.$element = $(element);
		this.$pics = $(element).find(options.picContainer).find(options.pics)
			.hide().first().show().end();
		this.$thumbs = $(element).find(options.thumbContainer)
			.on(options.action+'.simplegallery', options.thumbs, $.proxy(this.change, this))
			.find(options.thumbs);
	}

	SimpleGallery.prototype = {

		constructor: SimpleGallery

		,	change: function (e) {
			e.preventDefault();
			var $thumb = $(e.currentTarget);
			this.$pics.hide().eq(this.$thumbs.removeClass('active').index($thumb.addClass('active'))).show();
		}

	}

	$.fn.simplegallery = function (option) {
		return this.each(function () {
			var $this = $(this)
			,		data = $this.data('simplegallery')
			,		options = $.extend({}, $.fn.simplegallery.defaults, option);
			if (!data) $this.data('simplegallery', (data = new SimpleGallery(this, options)));
		});
	}

	$.fn.simplegallery.defaults = {
		picContainer: '.pics',
		pics: 'img',
		thumbContainer: '.thumbs',
		thumbs: 'img',
		action: 'hover',
		activeClass: 'active'
	};

	$.fn.simplegallery.Constructor = SimpleGallery;

	$(function () {
		$('[data-simplegallery]').each(function () {
			var $this = $(this);
			$this.simplegallery($this.data());
		})
	});

}(window.jQuery);